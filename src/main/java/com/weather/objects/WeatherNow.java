package com.weather.objects;

public class WeatherNow {

	private WeatherObject weatherObject;
	
	
	private WeatherHttpRequester weatherHttpRequester;
	private WeatherClient weatherClient;
	 
	private String xmlRespons = "";
	
	public WeatherNow() throws Exception{
		throw new ExceptionInInitializerError("WeatherNow needs jsonResponse!");
	}
	
	public WeatherNow(String cityName) throws Exception{
		
		weatherClient = new WeatherClient(cityName);
		String urlRequest = weatherClient.getFull_url();
		
		weatherHttpRequester = new WeatherHttpRequester(urlRequest);
		String xml = weatherHttpRequester.getResponse();
		this.setXmlResponse(xml);
		
		this.weatherObject = new WeatherObject(getXmlResponse());
	}

	public String getXmlResponse() {
		return xmlRespons;
	}

	public void setXmlResponse(String xmlRespons) {
		this.xmlRespons = xmlRespons;
	}
	
	public WeatherObject getWeatherObject(){
		return this.weatherObject;
	}
}
