package com.weather.objects;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLParser {


	private XPath xpath;

	public XMLParser() {
		XPathFactory xpf = XPathFactory.newInstance();
		xpath = xpf.newXPath();
		// xpath.setNamespaceContext(new PNameSpaceContext());

	}

	public Document newDocument(String validatingFile)
			throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		if (validatingFile.equals("") == false) {
			factory.setValidating(true);
			factory.setAttribute(
					"http://java.sun.com/xml/jaxp/properties/schemaLanguage",
					"http://www.w3.org/2001/XMLSchema");
			factory.setAttribute(
					"http://java.sun.com/xml/jaxp/properties/schemaSource",
					new File(validatingFile));
		}
		DocumentBuilder parser = factory.newDocumentBuilder();
		Document doc = parser.newDocument();
		return doc;
	}

	public String saveNode(Node node, String fileName)
			throws TransformerConfigurationException, TransformerException {
		String result = "";
		TransformerFactory f = TransformerFactory.newInstance();
		Transformer tr = f.newTransformer();
		DOMSource ds = new DOMSource(node);
		if (fileName.equalsIgnoreCase("")) {
			StreamResult ssr = new StreamResult(new StringWriter());
			tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			tr.transform(ds, ssr);
			StringWriter strwriter = (StringWriter) ssr.getWriter();
			StringBuffer strbuf = strwriter.getBuffer();
			result = strbuf.toString();
		} else {
			StreamResult sr = new StreamResult(new File(fileName));
			tr.transform(ds, sr);
		}
		return result;
	}

	public String saveDocument(Document doc, String fileName)
			throws TransformerConfigurationException, TransformerException {
		String result = "";
		TransformerFactory f = TransformerFactory.newInstance();
		Transformer tr = f.newTransformer();
		DOMSource ds = new DOMSource(doc);
		if (fileName.equalsIgnoreCase("")) {
			StreamResult ssr = new StreamResult(new StringWriter());

			tr.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			tr.transform(ds, ssr);
			StringWriter strwriter = (StringWriter) ssr.getWriter();
			StringBuffer strbuf = strwriter.getBuffer();
			result = strbuf.toString();
		} else {
			StreamResult sr = new StreamResult(new File(fileName));
			tr.transform(ds, sr);
		}
		return result;
	}

	public Document loadXML(String xml) throws ParserConfigurationException,
			SAXException, IOException {
		Document document = null;
		DocumentBuilder builder;
		builder = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			builder = factory.newDocumentBuilder();
			StringReader sr = new StringReader(xml);
			InputSource is = new InputSource(sr);
			try {
				document = builder.parse(is);
			} catch (SAXException e1) {
				e1.printStackTrace();

			} catch (IOException e2) {
				e2.printStackTrace();
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return document;
	}

	public NodeList selectNodes(Node node, String path)
			throws XPathExpressionException {
		NodeList list = null;
		XPathExpression expr = xpath.compile(path);
		list = (NodeList) expr.evaluate(node, XPathConstants.NODESET);
		return list;

	}

	public Node selectSingleNode(Node node, String path)
			throws XPathExpressionException {
		NodeList list = selectNodes(node, path);
		if (list.getLength() > 0) {
			return list.item(0);
		}
		return null;
	}

	public Node selectTheSingleNode(Node node, String path)
			throws XPathExpressionException {

		NodeList list = selectNodes(node, path);
		if (list.getLength() > 0) {
			return list.item(0).getFirstChild();
		}
		return null;
	}

	public String selectSingleNodeValue(Node node, String path)
			throws XPathExpressionException {
		Node resultNode = selectTheSingleNode(node, path);

		if (resultNode != null) {
			return resultNode.getNodeValue();
		}

		return null;
	}

}
